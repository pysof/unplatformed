```bash
#~ Content-Type: application/jake;pysof.org
```
# Patrick McKinnon
_est. 1980_

- Initiative: [Get rid of the G](https://www.grotg.com)
- Network: [LinkedIn](https://patrick.unplatformed.us/linkedin), [Nittwits](https://patrick.unplatformed.us/twitter)
- Words: [Medium](https://patrick.unplatformed.us/medium)
- Contact: [patrick@unplatformed.us](mailto:patrick@unplatformed.us)

![patrick](./patrick.png)

---
Legal Disputes: [com.pysof.org](http://com.pysof.org)
